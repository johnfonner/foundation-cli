#!/bin/bash
#
# clients-list
#
# author: dooley@tacc.utexas.edu
#
# This script is part of the Agave API command line interface (CLI).
# It retrieves a list of one or more registered clients.
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "$DIR/common.sh"
interactive=1

# Script logic -- TOUCH THIS {{{

# A list of all variables to prompt in interactive mode. These variables HAVE
# to be named exactly as the longname option definition in usage().
interactive_opts=(apiusername apipassword)

# Print usage
usage() {
  echo -n "$(basename $0) [OPTION]...
$(basename $0) [OPTION]... [CLIENT_NAME]

List all clients registered to the authenticated user.

 Options:
  -u, --apiusername		API apiusername
  -p, --apipassword		API apipassword
  -l, --limit         Maximum number of results to return
  -o, --offset        Number of results to skip from the start
  -H, --hosturl     	URL of the service
  -d, --development 	Run in dev mode using default dev server
  -f, --force       	Skip all user interaction
  -i, --interactive 	Prompt for values
  -q, --quiet       	Quiet (no output)
  -v, --verbose     	Verbose output
  -V, --veryverbose 	Very verbose output
  -h, --help        	Display this help and exit
      --version     	Output version information and exit
"
}

##################################################################
##################################################################
#						Begin Script Logic						 #
##################################################################
##################################################################

source "$DIR/clients-common.sh"

main() {
	#echo -n
	#set -x

	if [ -n "$args" ]; then
		clientsurl="${hosturl}${args}?pretty=true$(pagination)"
	else
		clientsurl="${hosturl}?pretty=true$(pagination)"
	fi

	cmd="curl -sku \"${apiusername}:xxxx\" ${clientsurl}"

	if ((veryverbose)); then
		[ "$piped" -eq 0 ] && log "Calling $cmd"
	fi

	response=`curl -sku "${apiusername}:${apipassword}" "${clientsurl}"`

	if [[ $(jsonquery "$response" "status") = 'success' ]]; then
		result=$(format_api_json "$response")
		success "$result"
	else
		errorresponse=$(jsonquery "$response" "message")
		err "$errorresponse"
	fi
}

format_api_json() {

	if ((veryverbose)); then
		echo "$1" | python -mjson.tool
	elif [[ $verbose -eq 1 ]]; then
		result=$(jsonquery "$1" "result" 1)
		echo "${result}" | python -mjson.tool
	elif [ -n "$args" ]; then
		result=$(jsonquery "$1" "result.name")
		echo "${result}"
	else
		result=$(jsonquery "$1" "result.[].name")
		echo "${result}"
	fi
}

##################################################################
##################################################################
#						End Script Logic						 #
##################################################################
##################################################################

# }}}

# Parse command line options
source "$DIR/options.sh"


# Main loop {{{

# Print help if no arguments were passed.
#[[ $# -eq 0 ]] && set -- "--help"

# Read the options and set stuff
while [[ $1 = -?* ]]; do
  case $1 in
    -h|--help) usage >&2; safe_exit ;;
    --version) version; copyright; disclaimer; safe_exit ;;
    -u|--apiusername) shift; apiusername=$1 ;;
    -p|--apipassword) shift; apipassword=$1;;
    -l|--limit) shift; limit=$1;;
    -o|--offset) shift; offset=$1;;
    -H|--hosturl) shift; hosturl=$1;;
  	-d|--development) development=1 ;;
    -v|--verbose) verbose=1 ;;
    -V|--veryverbose) veryverbose=1; verbose=1 ;;
    -q|--quiet) quiet=1 ;;
    -i|--interactive) interactive=1 ;;
    -f|--force) force=1 ;;
    --endopts) shift; break ;;
    *) die "invalid option: $1" ;;
  esac
  shift
done

# Store the remaining part as arguments.
args+=("$@")

# }}}

# Run the script logic
source "$DIR/runner.sh"
